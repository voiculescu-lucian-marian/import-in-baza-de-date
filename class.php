<?php
$host='localhost'; // Host Name.
$db_user= 'root'; //User Name
$db_password= '';
$db= 'oferta_furnizor';
 $connect = mysqli_connect($host, $db_user, $db_password, $db); // connect to database

 include ("PHPExcel/IOFactory.php");
 if(isset($_POST["Import"]) && $_FILES["file"]['size'] > 0)
{

 move_uploaded_file($_FILES["file"]["tmp_name"], "uploaded/".$_FILES["file"]["name"]); //files move in folder "uploaded"

 $html="<table  class='table' border='1'>";  
 $objPHPExcel = PHPExcel_IOFactory::load("uploaded/".$_FILES["file"]["name"]);
 
 foreach ($objPHPExcel->getWorksheetIterator() as $worksheet)   
 {  
      $highestRow = $worksheet->getHighestRow();  
      for ($row=2; $row<=$highestRow; $row++)  
      {  
           $html.="<tr>";
           $array_cod_produs[] = mysqli_real_escape_string($connect, $worksheet->getCellByColumnAndRow(0, $row)->getValue()); // create array with all 'cod produs' in imported file
           $cod_produs = mysqli_real_escape_string($connect, $worksheet->getCellByColumnAndRow(0, $row)->getValue());  
           $denumire = mysqli_real_escape_string($connect, $worksheet->getCellByColumnAndRow(1, $row)->getValue());
           $stoc = mysqli_real_escape_string($connect, $worksheet->getCellByColumnAndRow(2, $row)->getValue());  
           $pret_distributie = mysqli_real_escape_string($connect, $worksheet->getCellByColumnAndRow(3, $row)->getValue());
           $pret_recomandat_cu_tva = mysqli_real_escape_string($connect, $worksheet->getCellByColumnAndRow(4, $row)->getValue());  
           $pret_promo_fara_tva = mysqli_real_escape_string($connect, $worksheet->getCellByColumnAndRow(5, $row)->getValue());
           $pret_promo_cu_tva = mysqli_real_escape_string($connect, $worksheet->getCellByColumnAndRow(6, $row)->getValue());  
           $data = mysqli_real_escape_string($connect, $worksheet->getCellByColumnAndRow(7, $row)->getValue());
           $manufacturer = mysqli_real_escape_string($connect, $worksheet->getCellByColumnAndRow(8, $row)->getValue());
            $sql = "SELECT cod_produs FROM oferta_furnizor WHERE cod_produs= '".$cod_produs."'";
            $result = mysqli_query($connect, $sql);
            $data1 = mysqli_fetch_array($result, MYSQLI_NUM);
           
            if($data1[0] != NULL) {
              $sql_update = "UPDATE oferta_furnizor SET cod_produs='".$cod_produs."', denumire='".$denumire."', stoc='".$stoc."', pret_distributie='".$pret_distributie."', pret_recomandat_cu_tva='".$pret_recomandat_cu_tva."',  pret_recomandat_fara_tva='".$pret_promo_fara_tva."', pret_promo_cu_tva='".$pret_promo_cu_tva."', data_terminare_pp='".$data."', manufacturer='".$manufacturer."' WHERE cod_produs='".$cod_produs."'";
              mysqli_query($connect, $sql_update);
            } else {
           $sql_insert = "INSERT INTO oferta_furnizor(cod_produs, denumire, stoc, pret_distributie, pret_recomandat_cu_tva, pret_recomandat_fara_tva, pret_promo_cu_tva, data_terminare_pp, manufacturer) VALUES ('".$cod_produs."', '".$denumire."', '".$stoc."', '".$pret_distributie."', '".$pret_recomandat_cu_tva."', '".$pret_promo_fara_tva."', '".$pret_promo_cu_tva."', '".$data."', '".$manufacturer."')";
           mysqli_query($connect, $sql_insert);
            }
              if($cod_produs == NULL && $denumire != NULL) {
                echo "Produsul cu denumirea '".$denumire."' nu are Cod produs <br/>";
              };
              if ($stoc == NULL && $denumire != NULL) {
                echo "Produsul cu denumirea '".$denumire."' nu are Stoc <br/>";
              };
              if ($pret_distributie == NULL && $denumire != NULL) {
                echo "Produsul cu denumirea '".$denumire."' nu are Pret distributie RON fara TVA <br/>";
              };
              if ($pret_recomandat_cu_tva == NULL && $denumire != NULL) {
                echo "Produsul cu denumirea '".$denumire."' nu are Pret recomandat RON cu TVA <br/>";
              };
              if ($pret_promo_fara_tva == NULL && $denumire != NULL) {
                echo "Produsul cu denumirea '".$denumire."' nu are Pret promo RON fara TVA <br/>";
              };
              if ($pret_promo_cu_tva == NULL && $denumire != NULL) {
                echo "Produsul cu denumirea '".$denumire."' nu are Pret Promo recomandat RON cu TVA <br/>";
              };
              if ($data == NULL && $denumire != NULL) {
                echo "Produsul cu denumirea '".$denumire."' nu are Data Terminare PP <br/>";
              };
              if ($manufacturer == NULL && $denumire != NULL) {
                echo "Produsul cu denumirea '".$denumire."' nu are Manufacturer <br/>";
              };
           $html.= '<td>'.$cod_produs.'</td>';  
           $html .= '<td>'.$denumire.'</td>';
           $html.= '<td>'.$stoc.'</td>';  
           $html .= '<td>'.$pret_distributie.'</td>';
           $html.= '<td>'.$pret_recomandat_cu_tva.'</td>';  
           $html .= '<td>'.$pret_promo_fara_tva.'</td>';
           $html.= '<td>'.$pret_promo_cu_tva.'</td>';  
           $html .= '<td>'.$data.'</td>';
           $html .= '<td>'.$manufacturer.'</td>';  
           $html .= "</tr>";  
      }  
 }  
 $html .= '</table>';  
 

    if(isset($sql_insert) && isset($sql_update)) {
      if(count($sql_insert)!=0 && count($sql_update)!=0) {
        echo 'Data Inserted and updated<br />';
      }
    } elseif(isset($sql_insert)) {
        echo 'Data Inserted<br />';
    } elseif(isset($sql_update)) {
        echo 'Data Updated<br />';
    }
      else {
        echo 'No data to insert or update<br />';
    }

    if(isset($array_cod_produs)) {
      $sql_delete = "DELETE FROM oferta_furnizor WHERE cod_produs NOT IN('".implode("','",$array_cod_produs)."');";
      $result_delete = mysqli_query($connect, $sql_delete);

      if (count(array_unique($array_cod_produs)) !== count($array_cod_produs)) {
        echo "There are duplicates";
      } elseif(count(array_unique($array_cod_produs)) === count($array_cod_produs)) {
        echo "No duplicates";
      }
        else {

      }
    } else {
      echo "Uploaded file empty!";
    }

    echo $html; //display inserted and updated data into table

} elseif (isset($_POST["Import"]) && $_FILES["file"]['size'] == 0) {
    echo "No file selected for upload. Please select a file";
}

 ?>