-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 21, 2016 at 05:04 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `oferta_furnizor`
--

-- --------------------------------------------------------

--
-- Table structure for table `oferta_furnizor`
--

CREATE TABLE `oferta_furnizor` (
  `cod_produs` varchar(20) NOT NULL,
  `denumire` varchar(255) NOT NULL,
  `stoc` varchar(20) NOT NULL,
  `pret_distributie` int(20) DEFAULT NULL,
  `pret_recomandat_cu_tva` int(20) NOT NULL,
  `pret_recomandat_fara_tva` int(20) NOT NULL,
  `pret_promo_cu_tva` int(20) DEFAULT NULL,
  `data_terminare_pp` datetime NOT NULL,
  `manufacturer` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oferta_furnizor`
--

INSERT INTO `oferta_furnizor` (`cod_produs`, `denumire`, `stoc`, `pret_distributie`, `pret_recomandat_cu_tva`, `pret_recomandat_fara_tva`, `pret_promo_cu_tva`, `data_terminare_pp`, `manufacturer`) VALUES
('G1G90SRSE', 'Balansoar Sweetpeace -Stars', '2', 708, 976, 0, 0, '0000-00-00 00:00:00', 'Graco'),
('G1G92ZENE', 'Balansoar Sweetpeace - Zen', 'in stoc', 708, 976, 0, 0, '0000-00-00 00:00:00', 'Graco'),
('G1H95CCUE', 'Balansoar Baby Delight - Circus', 'in stoc', 229, 316, 0, 0, '0000-00-00 00:00:00', 'Graco'),
('G1H95GAFE', 'Balansoar Baby Delight - Garden Friends', '1', 229, 316, 0, 0, '0000-00-00 00:00:00', 'Graco'),
('G1L98NSDE', 'Balansoar Lovin Hug cu conectare la priza - Neon Sand', 'in stoc', 401, 553, 0, 0, '0000-00-00 00:00:00', 'Graco'),
('G1R50BABE', 'Balansoar Sweetsnuggle - Benny Bell', 'in stoc', 438, 604, 0, 0, '0000-00-00 00:00:00', 'Graco'),
('G4G99SRIE', 'Balansoar Duet Rocker - Birdies', 'in stoc', 300, 414, 0, 0, '0000-00-00 00:00:00', 'Graco'),
('G8E397ALIE', 'Scaun auto Assure - Aluminium', 'in stoc', 362, 499, 0, 0, '0000-00-00 00:00:00', 'Graco'),
('G8E79BKSB', 'Scaun auto Logico L X Comfort - Black Stone', 'in stoc', 0, 447, 0, 0, '0000-00-00 00:00:00', 'Graco'),
('G8E89FTBE', 'Scaun auto Junior Maxi Football', 'in stoc', 181, 250, 0, 0, '0000-00-00 00:00:00', 'Graco'),
('G8E93PECE', 'Scaun inaltator pentru copii - Peacoat', '1', 103, 142, 0, 0, '0000-00-00 00:00:00', 'Graco'),
('G8F97SLXE', 'Scaun auto Junior Baby  - Sport Luxe', '2', 455, 627, 0, 0, '0000-00-00 00:00:00', 'Graco'),
('G8J53ALIE', 'Scaun auto Nautilus - Aluminium', 'fara stoc', 624, 862, 0, 0, '0000-00-00 00:00:00', 'Graco'),
('G8J54DBLE', 'Scaun auto Nautilus - Diablo', 'in stoc', 624, 862, 0, 0, '0000-00-00 00:00:00', 'Graco'),
('G8J95BKSE', 'Scaun auto Nautilus - Black Stone', 'fara stoc', 624, 862, 0, 0, '0000-00-00 00:00:00', 'Graco'),
('G9AF99PCEU', 'Patut Nimble Nook Pierce', 'in stoc', 386, 532, 359, 496, '2016-11-23 00:00:00', 'Graco'),
('MBP16', 'Interfon digital Motorola MBP16', 'in stoc', 196, 270, 181, 249, '2016-12-31 00:00:00', 'Motorola');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `oferta_furnizor`
--
ALTER TABLE `oferta_furnizor`
  ADD UNIQUE KEY `cod_produs` (`cod_produs`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
